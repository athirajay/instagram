import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import ReadMore from 'vue-read-more';
import App from './App.vue'


Vue.use(ReadMore);
Vue.use(BootstrapVue)


new Vue({
  el: '#app',
  render: h => h(App)
})
